(ns demo.core-test
  (:require [cljs.test :refer-macros [deftest is testing run-tests async]]
            [cljs.core.async :refer [<! >! timeout take! chan] :refer-macros [go go-loop]]))

(deftest native-resolve
  (async done
         (.finally
          (.then (js/Promise.resolve 42)
                 #(is (= %1 42))
                 #(is false "Should not reject"))
          (done))))

(deftest slow-test
  (async done
         (js/setTimeout (fn [] (is true) (done)) 1000)))
