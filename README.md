# Demo Reframe

## Run

``` shell
yarn install

yarn dev
```

## Clean

``` shell
yarn clean
```

## Release

To bundle our app into javascript, it will generate a javascript file
at this path: `public/js/compiled/main.js`

``` shell
yarn release
```

To run our clojure backend:

``` shell
clj -A:dev
```




--------------------------------------------------------------

### connect now with cider

    cider-jack-in-cljs

# choose:

    shadow-cljs

# selection option:

    shadow

# select

    :app
