(defproject demo "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/clojurescript "1.10.773"]
                 [org.clojure/core.match "0.3.0"]
                 [metosin/reitit "0.3.10"]
                 [reagent "0.8.1"]
                 [re-frame "0.10.9"]
                 [ring/ring-jetty-adapter "1.7.1"]]

  :min-lein-version "2.5.3"

  :source-paths ["src/clj"]

  :resource-paths ["resources/public"]

  :clean-targets ^{:protect false} ["target"]

  :profiles {:dev {:dependencies [[binaryage/devtools "0.9.10"]
                                  [cider/piggieback "0.4.0"]]
                   :main backend.core/-main}})
