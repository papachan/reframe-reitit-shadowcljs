(ns backend.handler
  (:require [clojure.java.io :as io]
            [ring.util.response :as resp]
            [ring.middleware.content-type :as content-type]))

(def handler
  (-> (fn [request]
        (or (resp/resource-response (:uri request) {:root "public"})
            ;; incase you have public at root remove {:root "public"}
            (-> (resp/resource-response "index.html" {:root "public"})
                (resp/content-type "text/html"))))
      content-type/wrap-content-type))
