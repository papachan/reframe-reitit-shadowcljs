(ns backend.core
  (:require [ring.adapter.jetty :refer [run-jetty]]
            [reitit.ring :as ring]
            [backend.handler :refer [handler]]
            [ring.util.response :as resp]))

;; (defn handler [_]
;;   {:status 200, :body "ok"})

(def app
  (ring/ring-handler
    (ring/router
      [["/" {:get handler}]])
    (ring/routes
      (ring/create-resource-handler {:path "/" :root "/public"})
      (ring/create-default-handler))))

(defn start
  []
  (run-jetty app {:port 3000 :join? false})
  (println "server running in port 3000"))

(defn -main
  [& args]
  (start))

(comment
  (start))
